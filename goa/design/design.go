package design

import (
    . "github.com/goadesign/goa/design"
    . "github.com/goadesign/goa/design/apidsl"
)

var _ = API("", func() {
        Title("")
        Description("")
        Host("")
        Scheme("")
})

var _ = Resource("", func() {
    Action("", func() {
        Routing(GET("/"))
        Description("")
        Params(func() {
            Param()
            Param()
        })
    Response(OK, "text/plain")
    })
})
